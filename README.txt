
DESCRIPTION
-----------
This module allows you to, for each group type, specify a list of roles
that group administrators are allowed to assign. In the subscriber list
(og/users/<node id>), a 'configure member roles' tab will appear if both
the group type is allowed to configure roles and the current user is an
admin for the group.

For example, if you created a "trusted user" role, and a new node type
called "official group," you could allow administrators of only official 
groups to add selected other group members of to the "trusted user" role.

REQUIREMENTS
------------
- Requires the organic groups module.

INSTALLATION
------------
- Enable the module from administer >> modules.

USAGE
-----
- Go to administer >> access >> roles and add additional roles that you
  wish to be managed by group administrators.
- Assign appropriate privileges to said roles. Note that because these will
  be assignable by non-site admins, you should be conservative in what 
  permissions you give (maybe 'create' permissions on a special node type, etc.)
- Go to administer >> settings >> og_roles and check the box next to each
  role you wish group admins to be able to manage.
- In a group with permissions 

NOTES
-----
- Please note that these roles and their permissions will take effect site-wide, 
  NOT only within each group!

CREDITS
-------
Newly mantained by Wojtha (since 5.0)
Authored and maintained (until 4.7) by Farsheed Hamidi-Toosi and Angela Byron of CivicSpace Labs
Sponsored by Raven Brooks of BuyBlue.org
